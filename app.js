/*
A chatbot by lumos309
 */

'use strict';

const cookies = require("browser-cookies");
const request = require("request-promise");

const telegramBot = require('node-telegram-bot-api');
const express = require('express');
const structjson = require('./structjson.js'); // for error handling in context parsing
const constants = require('./constants.js'); // import pre-defined constants
const functions = require('./functions.js'); // import helper functions
const admin = require("firebase-admin"); // firebase admin sdk
const serviceAccount = require("./rvrc-bot-firebase-adminsdk-siu54-1df1274b90.json"); // firebase credentials
const dialogflow = require('dialogflow');
const uuid = require('uuid');

const aboutMessage = constants.aboutMessage;
const helpMessage = constants.helpMessage;
const startMessage = constants.startMessage;
const yesNoKeyboard = constants.yesNoKeyboard;
const creditsURL = constants.creditsURL;
const menuURL = constants.menuURL;
const formatAcadDate = functions.formatAcadDate;
const datesToDays = functions.datesToDays;
const getAndParseMenu = functions.getAndParseMenu;
const formatDateMenu = functions.formatDateMenu;
const checkTimeout = functions.checkTimeout;
const sleep = functions.sleep;

///* express routing setup *///
const app = express();

// cron job - ping every 2 mins to maintain instance
app.get('/', (req, res) => {
  res
    .status(200)
    .send('Hello, world!')
    .end();
});


// cron job - restart bot polling every 10 mins
// workaround? It dies randomly for no reason...
app.get('/restart', (req, res) => {
	/*
	bot = null;
	sleep(10000);
	bot = new telegramBot(token, {polling: true});
	*/
	
	bot.stopPolling();
	sleep(2000);
	bot.startPolling();
	//console.log("Restarted bot.");
	
	res
		.status(200)
		.send("Restarted bot.")
		.end();
});


// cron job - ping every 60 mins to clear sessions >=20 mins old
app.get("/timeoutCheck", (req, res) => {
	let sessionDeleteCount = 0;
	let mealCacheDeleteCount = 0;
	let itemCacheDeleteCount = 0;
	
	for (const i of Object.keys(activeSessions)) {
		if (checkTimeout(activeSessions[i])) {
			delete activeSessions[i];
			sessionDeleteCount += 1;
		}
	}
	for (const i of Object.keys(mealServicesCache)) {
		if (checkTimeout(mealServicesCache[i])) {
			delete mealServicesCache[i];
			mealCacheDeleteCount += 1;
		}
	}
	for (const i of Object.keys(foundItemCache)) {
		if (checkTimeout(foundItemCache[i])) {
			delete foundItemCache[i];
			itemCacheDeleteCount += 1;
		}
	}
	if ((sessionDeleteCount > 0) && (mealCacheDeleteCount > 0) && (itemCacheDeleteCount > 0)) {
		console.log(`Deleted ${sessionDeleteCount} sessions, ${mealCacheDeleteCount} meal entries, ${itemCacheDeleteCount} item entries.`);
	}
	
	res
		.status(200)
		.send(`Timeoutcheck done.`)
		.end();
});

// cron job - periodically send updates for each date/meal
app.get("/sendAttendance/:meal", async function (req, res) {
	// send 3 (placeholder) days in advance
	const meal = req.params.meal;
	const currDate = new Date(Date.now());
	const adminId = 500326343; // placeholder
	currDate.setDate(currDate.getDate() + 3);
	const targetDate = formatDateRangeDb(currDate, currDate)[0][0];
	
	const result = await getDailyStats(meal, targetDate);
	bot.sendMessage(adminId, `${result} cancellation${result === 1 ? '': 's'} for ${targetDate} ${meal}.`);
	
	res
		.status(200)
		.send(`Attendance for ${targetDate} ${meal} sent.`)
		.end();
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

///* firebase database setup *///
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://rvrc-bot.firebaseio.com",
  databaseAuthVariableOverride: {
    uid: "master-account"
  }
});

const db = admin.database();
const attRef = db.ref("attendance");

// database access functions
async function updateAtt(date, meal, change) {
	const tempRef = attRef.child(date).child(meal);
	var result = 0;
	await tempRef.once("value", function(snapshot) {
		if (snapshot.val()) {
			result = snapshot.val();
		}
	});
	result += change;
	await tempRef.set(result);
}

async function getUserStatus(userId) {
	const tempRef = db.ref(`userRecords/${userId}`);
	const result = {};
	await tempRef.orderByValue().once("value", function(snapshot) {
	  snapshot.forEach(function(data) {
		result[data.key] = data.val();
	  });
	});
	return result;
}

async function checkAndUpdateStatus(userId, date, meal, newStatus) {
	const tempRef = db.ref(`userRecords/${userId}/${date}/${meal}`);
	let result = true;
	
	await tempRef.once("value", async function(snapshot) {
		if (snapshot.val()) {
			// user has cancelled meal before
			if (newStatus === -1) {
				await tempRef.set(null);
				result = true;
			} else {
				result = false;
			}
		} else {
			if (newStatus === 1) {
				await tempRef.set(1);
				result = true;
			} else {
				result = false;
			}
		}
	});
	return result;
}

async function searchForItem(itemType, itemColour = null, lostLocation = null) {
	const tempRef = db.ref(`searchItems/${itemType}`);
	let result = null;
	await tempRef.once("value", function(snapshot) {
		result = snapshot.val();
	});
	return result;
}

async function addFoundItem(userId) {
	// each chatId can only have one item stored at once
	const itemObject = foundItemCache[userId].data;
	const tempRef = db.ref(`searchItems/${itemObject.itemType}`);
	await tempRef.push(itemObject.itemData);
	delete foundItemCache[userId];
}

async function getDailyStats(meal, date) {
	const tempRef = attRef.child(date).child(meal);
	var result = 0;
	await tempRef.once("value", function(snapshot) {
		if (snapshot.val()) {
			result = snapshot.val();
		}
	});
	return result;
}

async function registerAdmin(id, name) {
	const tempRef = db.ref(`mealAdmin/${id}`);
	await tempRef.set(name);
}

async function addFeedback(id, food, feedback) {
    const tempRef = db.ref(`menuFeedback/${food}/${id}`);
    await tempRef.set(feedback);
}

///* main *///

const activeSessions = {};
const mealServicesCache = {};
const foundItemCache = {};
const menuFeedbackCache = {};

// replace the value below with the Telegram token you receive from @BotFather
// live
// const token = '685141726:AAHEhQ1c3hgdtYnUySvjsi1RsEHD7W3UXzY';
// testing
const token = '595968748:AAFJ1rD_g2SmuGyq3KvqU9VV6HPIF7W-F2I';

// Create a bot that uses 'polling' to fetch new updates
let bot = new telegramBot(token, {polling: true});

// Matches "/echo [whatever]"
bot.onText(/\/echo (.+)/, (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
	console.log(msg.chat.id);
	console.log(msg.chat);
  const chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', (msg) => {
    const chatId = msg.chat.id;
	
	switch(msg.text) {
		
		case "/about":
			bot.sendMessage(chatId, aboutMessage);
			break;
		
		case "/help":
			bot.sendMessage(chatId, helpMessage);
			break;
			
		case "/start":
			bot.sendMessage(chatId, startMessage);
			break;
			
		case "testFn":
			bot.sendMessage(chatId, creditsURL);
			break;
			
		case "next":
			const URL = "https://aces.nus.edu.sg/Prjhml/login.do?code=9RRlpckOCken5eIsfqavzQ.5UwSgSZ61gjQFhQ0RQORb9gSar4.pXAkkrJ5qmqGQnDaOcoinAE-Los219VDtiBgWEDVG-5_F5di13UxIPahqeFxFg5O4IDjiJb6gx2Ay5-A55VtGduP0f-wCtnZFBADl0hOwW9X77GzzU9lx83KzNplAGC_e-P4bdPEXQ3bZAt6G19kDMxULWkBn4bh7FTr6EhB3JG-D6aluFZK5XYYjo_kr6re08eMyQtCuXXwZ_3mhSlbNm5TkEK4_2AOLJ06YqgDRwnRxZo4caC2mnqA7ZA30GVyYgh-YFWCejosIJepCzk32DcyydjCUlpcSm-lgWh1GW-S9QBZt0jWdKC3ikC-O1exTMoo3A3bLYyTONQ8WINcAw#;jesssionid=GwGVoEDh9O6A2YNrNtXZrcQCNbTacIUzki-0jQq-.cwais007";
			request(URL) // 
				.then(function (htmlString) {
					const body = htmlString;
					console.log(body);
				})
				.catch(function (err) {
					console.log(err);
				});
			break;
	
		default:
			awaitAndSendResponse(msg).catch(err => console.error(`Error awaitAndSendResponse: ${err}`));
	
	}
});

bot.on('callback_query', (query) => {
	const queryId = query.id;
	const callbackData = query.data.split(' ');
	bot.answerCallbackQuery(queryId);

	processCallbackQuery(query);
});

async function awaitAndSendResponse(msg) {
	const chatId = msg.chat.id;
	bot.sendChatAction(chatId, "typing");
	const projectId = 'rvrc-bot';

	const sessionClient = new dialogflow.SessionsClient();
	let sessionPath = null;
	if (activeSessions[chatId]) {
		sessionPath = activeSessions[chatId].sessionPath;
		activeSessions[chatId] = {time: Date.now(),
								  sessionPath: sessionPath};
	} else {
		const sessionId = uuid.v4();
		sessionPath = sessionClient.sessionPath(projectId, sessionId);
		activeSessions[chatId] = {time: Date.now(),
								  sessionPath: sessionPath};
	}

	// The text query request.
	const request = {
		session: sessionPath,
		queryInput: {
			text: {
				text: msg.text,
				languageCode: 'en-US',
			},
		},
	};
	
	// Send request and log result
	const responses = await sessionClient.detectIntent(request);
	//console.log('Detected intent');
	const result = responses[0].queryResult;
	/*console.log(`  Query: ${result.queryText}`);
	console.log(`  Response: ${result.fulfillmentText}`);
	if (result.intent) {
		console.log(`  Intent: ${result.intent.displayName}`);
	} else {
		console.log(`  No intent matched.`);
	}*/
	
	// initialise text response to send back to user
	var responseText;
	var responseOptions = {parse_mode: "Markdown"};
	var sendingStyle = null;
	if (result.fulfillmentText) {
		responseText = result.fulfillmentText;
	}

	// if matched intent contains an action, call processAction
	if (result.action) {
		const actionResponse = await processAction(responses, chatId);
		
		// update each field only if processAction returns a non-null value
		if (actionResponse.message) {
			responseText = actionResponse.message;
		}
		if (actionResponse.options) {
			responseOptions = actionResponse.options;
		}
		if (actionResponse.sendingStyle) {
			sendingStyle = actionResponse.sendingStyle;
		}
	}
	
	/* Not currently needed
	// console.log(responses[0].queryResult.outputContexts);
	responses[0].queryResult.outputContexts.forEach(context => {
	  // There is a bug in gRPC that the returned google.protobuf.Struct
	  // value contains fields with value of null, which causes error
	  // when encoding it back. Converting to JSON and back to proto
	  // removes those values.
	  context.parameters = structjson.jsonToStructProto(
		structjson.structProtoToJson(context.parameters)
	  );
	});
	*/

	// send message back to Telegram
	if (sendingStyle === "sendAsJoke") {
		sendMultipleMessages(chatId, responseText, 2000);
	} else {
		bot.sendMessage(chatId, responseText, responseOptions);
	}
}

/* 
Handles processing for all intents that return an action.
Returns an object in the following form:
{
 responseText: string containing message to send,
 responseOptions: object corresponding to additional sendMessage parameters,
[sendingStyle]: string describing additional options (e.g. send as multiple messages)
}
*/
async function processAction(responses, id) {
	let result = responses[0].queryResult;
	const inputParams = result.parameters.fields;
	let responseText = '';
	let sendingStyle = null;
	let responseOptions = {parse_mode: "Markdown"};
	switch (result.action) {
		
		case "verify-meal-and-date":
		case "verify-meal-and-date-restore":

			// parse input and store selection in cache
			const mealServices = [], datesOrRanges = [], cacheData = [];
			for (const i in inputParams['meal-service'].listValue.values) {
				mealServices.push(inputParams['meal-service'].listValue.values[i].stringValue);
			}
			if (inputParams['repeat-weeks'].stringValue || inputParams['acad-calendar-week'].listValue.values.length > 0) {
				// "every (even/odd) week"               || "reading week", "week 3 and/to 5"
				const inputWeeks = inputParams['repeat-weeks'].stringValue
									? inputParams['repeat-weeks'].stringValue
									: inputParams['acad-calendar-week'].listValue.values;
				const inputDays = inputParams['date-or-range'].listValue.values;
				const conjunction = inputParams.conjunction.stringValue;
				const days = datesToDays(inputDays);
				const parseDateResult = formatAcadDate(inputWeeks, days, conjunction);
				for (const i in parseDateResult) {
					const currentWeek = [];
					currentWeek.push(parseDateResult[i][0]);
					for (let j = 1; j < parseDateResult[i].length; j++) {
						currentWeek.push(formatDateMsg(parseDateResult[i][j]));
						cacheData.push([formatDateRangeDb(parseDateResult[i][j], parseDateResult[i][j])[0], mealServices[0]]);
					}
					datesOrRanges.push(currentWeek);
				}

			} else {
				// "next Mon", "1st Jan"
				const datesOrRangesResult = inputParams['date-or-range'].listValue.values;
				for (const i in datesOrRangesResult) {
					let dateList;
					if (datesOrRangesResult[i].structValue.fields.date) {
						datesOrRanges.push(formatDateMsg(datesOrRangesResult[i].structValue.fields.date.stringValue));
						dateList = formatDateRangeDb(datesOrRangesResult[i].structValue.fields.date.stringValue,
													 datesOrRangesResult[i].structValue.fields.date.stringValue);
					} else {
						const startDate = formatDateMsg(datesOrRangesResult[i].structValue.fields['date-period'].structValue.fields.startDate.stringValue);
						const endDate = formatDateMsg(datesOrRangesResult[i].structValue.fields['date-period'].structValue.fields.endDate.stringValue);
						datesOrRanges.push([startDate, endDate]);
						dateList = formatDateRangeDb(datesOrRangesResult[i].structValue.fields['date-period'].structValue.fields.startDate.stringValue,
													 datesOrRangesResult[i].structValue.fields['date-period'].structValue.fields.endDate.stringValue);
					}
					if (mealServices[i] === "breakfast and dinner") {
						for (const j in dateList) {
							cacheData.push([dateList[j], "breakfast"]);
							cacheData.push([dateList[j], "dinner"]);
						}
					} else {
						for (const j in dateList) {
							cacheData.push([dateList[j], mealServices[i]]);
						}
					}
				}
			}
			const timeNow = Date.now();
			mealServicesCache[id] = {time: timeNow,
									 data: cacheData};
			
			// format response text
			if (inputParams['repeat-weeks'].stringValue || inputParams['acad-calendar-week'].listValue.values.length > 0) {
				let responseString = `You are *${result.action === "verify-meal-and-date"
													? "cancelling "
													: "restoring "}`
									  + `${mealServices[0]}* on the following days:\n\n`;
				for (const i in datesOrRanges) {
					responseString += `*${datesOrRanges[i][0]}:*\n`;
					for (let j = 1; j < datesOrRanges[i].length; j++) {
						responseString += datesOrRanges[i][j].day + ', ' + datesOrRanges[i][j].date + '\n';
					}
				}
				responseText = responseString + "\nIs that correct?";
			} else {
				const responseStrings = [];
				for (const i in mealServices) {
					let tempStr = mealServices[i];
					tempStr += Array.isArray(datesOrRanges[i])
							       ? ` from ${datesOrRanges[i][0].day}, ${datesOrRanges[i][0].date}`
								     + ` to ${datesOrRanges[i][1].day}, ${datesOrRanges[i][1].date}`
								   : ` on ${datesOrRanges[i].day}, ${datesOrRanges[i].date}`;
					responseStrings.push(tempStr);
				}
				let responseString = `You are *${result.action === "verify-meal-and-date"
													? "cancelling* "
													: "restoring* "}`;
				if (responseStrings.length === 1) {
					responseString += responseStrings[0] + '.';
				} else {
					const lastItem = responseStrings.pop();
					responseString += responseStrings.join(', ') + " and " + lastItem + ".";
				}
				responseText = responseString + " Is that right?";
			}
			responseOptions.reply_markup = yesNoKeyboard;
			
			break;

		case "update-attendance":
		case "update-attendance-restore":

			// change is a value (+/- 1) that is added directly to the database
			const change = result.action === "update-attendance"
								? 1
								: -1;

			// check and update (if applicable) each meal/date combination
			let successStr = "", failedStr = "", invalidStr = "", expiredStr = "";
			const meals = mealServicesCache[id].data;

			for (const i in meals) {
				const currDate = meals[i][0];
				const currMeal = meals[i][1];
				const dateStr = formatDateMsg(currDate[0]);
				// first check that the meal/day combination is valid
				if ((currMeal === "dinner" && currDate[1] === 6) ||    // Saturday dinner
					(currMeal === "breakfast" && currDate[1] === 0)) { // Sunday breakfast
					invalidStr += `${dateStr.day}, ${dateStr.date}, ${currMeal}\n`;
				} else if (new Date(currDate[0]) < Date.now()) {
				// next check if date is "expired" (before current date)
					expiredStr += `${dateStr.day}, ${dateStr.date}, ${currMeal}\n`
				} else if (await checkAndUpdateStatus(id, currDate[0], currMeal, change)) {
					updateAtt(currDate[0], currMeal, change).catch(err => (console.error(`Err updateAtt: ${err}`)));
					successStr += `${dateStr.day}, ${dateStr.date}, ${currMeal}\n`;
				} else {
					failedStr += `${dateStr.day}, ${dateStr.date}, ${currMeal}\n`;
				}
			}
			
			// format response to user
			if (failedStr === "" && invalidStr === "" && expiredStr === "") {
				responseText = "Success! All requested updates were recorded:\n" + successStr;
			} else {
				if (invalidStr !== "") {
					responseText += "Note: There is no meal service for the following:\n" + invalidStr + '\n';
				}
				if (expiredStr != "") {
					responseText += "Note: The following meal service(s) is/are in the past:\n"
									+ expiredStr + '\n';
				}
				if (failedStr !== "") {
					responseText += "Note: You've already previously cancelled/restored the following meals:\n"
									+ failedStr + '\n';
				}
				if (successStr === "") {
					responseText += "No changes were recorded.";
				} else {
					responseText += "All other requested updates were recorded:\n" + successStr;
				}
			}
			delete mealServicesCache[id];

			break;	
		
		case "retrieve-meals":

			const cancelledMeals = await getUserStatus(id);
			let listOfMeals = '';
			for (const i in Object.keys(cancelledMeals)) {
				const date = Object.keys(cancelledMeals)[i];
				const meals = cancelledMeals[date];
				const dateStr = formatDateMsg(date);
				listOfMeals += `${dateStr.day}, ${dateStr.date}`
				if (Object.values(meals).length === 2) {
					listOfMeals += ": Breakfast, Dinner\n";
				} else if (meals.breakfast) {
					listOfMeals += ": Breakfast\n";
				} else {
					listOfMeals += ": Dinner\n";
				}
			}
			if (listOfMeals === '') {
				responseText = "No recorded cancelled meals currently.";
			} else {
				responseText = "You have currently cancelled the following meals:\n" + listOfMeals;
			}
			break;

		case "register-admin":
			const name = result.queryText.split(' ')[1];
			try {
				await registerAdmin(id, name);
				responseText += "Success! You are now registered as an admin and will receive updates for each meal.";
			} catch (err) {
				console.log(err);
				responseText += "Sorry, that failed. Please contact @lumos309 for help.";
			}
			break;

		case "display-menu":
			const meal = inputParams['meal-service'].stringValue;
			let date;
			if (inputParams.date.stringValue) {
				date = inputParams.date.stringValue;
			} else {
				date = new Date(Date.now());
				// if time > 1600, increment date (desired date in GMT+8 is the next day)
				if (date.getHours() >= 16) date.setDate(date.getDate() + 1);
			}
			const dateStr = formatDateMsg(date);
			date = formatDateMenu(date);
			const menuResponse = await getAndParseMenu(date, meal, "fullMenu");
			responseText = menuResponse === ''
				? `Oops! There is no ${meal} service on ${dateStr.day}, ${dateStr.date}.`
				: meal.slice(0, 1).toUpperCase() + meal.slice(1)
				  + ` on ${dateStr.day}, ${dateStr.date}:\n\n` + menuResponse;
			break;
			
		case "prompt-station-selection":
            /*
            Parse input params. 
            If no date specified, default to current day.
            If no meal specified, default to breakfast (before 1730) or dinner (1730 onwards).
            */
            let feedbackDate, feedbackMeal;
            let dateNow = new Date(Date.now());
            dateNow.setHours(dateNow.getHours() + 8); // convert to GMT +8
            feedbackDate = inputParams.date.stringValue !== ''
                           ? formatDateMenu(inputParams.date.stringValue)
                           : formatDateMenu(dateNow);
            feedbackMeal = inputParams['meal-service'].stringValue !== ''
                           ? inputParams['meal-service'].stringValue
                           : ((dateNow.getHours() >= 17) && (dateNow.getMinutes() >= 30))
                                ? "dinner"
                                : "breakfast";
			const menuComponents = await getAndParseMenu(feedbackDate, feedbackMeal, "menuComponents");
			const stationsKeyboard = {
								         inline_keyboard: []
									  };
			menuFeedbackCache[id] = menuComponents[1];
            let i = 0;
            while (menuComponents[0].length >= 2) {
                const temp = [];
                temp.push({
                    text: menuComponents[0].shift(),
                    callback_data: "menuComponent " + i
                });
                i++;
                temp.push({
                    text: menuComponents[0].shift(),
                    callback_data: "menuComponent " + i
                });
                i++;
                stationsKeyboard.inline_keyboard.push(temp);
            }
            if (menuComponents[0].length === 1) {
                stationsKeyboard.inline_keyboard.push([{
                    text: menuComponents[0].shift(),
                    callback_data: "menuComponent " + i
                }]);
                i++;
            }
            responseOptions.reply_markup = stationsKeyboard;
            responseText = "Please select the station you'd like to provide feedback for.";
			
			break;
			
		case "log-feedback":
            const menuChoice = result.outputContexts[0].parameters.fields.number.numberValue;
            const menuItem = menuFeedbackCache[id][menuChoice];
            addFeedback(id, menuItem, result.queryText);
            responseText = "Success! Your feedback was recorded.";
            break;
			
		/*
		case "login":
			const jID = getJSessionID();
			const url = creditsURL + "login.do;jsessionid=" + jID;
			responseText = "Login at the following link: " + url;
			break;
	
		case "check-credit-balance":
			break;

		case "verify-found-item-entry":

			console.log(result);
			// parse input
			const item = inputParams["search-items"].stringValue;
			const foundLocation = inputParams["rvrc-location"].listValue.values[0].stringValue;
			let storedLocation = null;
			if (inputParams["rvrc-location"].listValue.values.length === 2) {
				storedLocation = inputParams["rvrc-location"].listValue.values[1].stringValue;
			}
			let colour = null;
			if (result.parameters.color) {
				colour = result.parameters.colour;
			}
			const fullDetails = result.queryText;

			// format response
			responseText = `These are the details that will be saved:\n\nItem: ${item}\n`;
			if (colour) {
				responseText += `Colour: ${colour}\n`;
			}
			responseText += `Found in: ${foundLocation}\n`;
			if (storedLocation) {
				responseText += `Currently kept in: ${storedLocation}\n`;
			}
			responseText += `Additional Details: ${fullDetails}\n\nIs this right?`;
			responseOptions.reply_markup = yesNoKeyboard;

			// create object in database storage format and save locally
			const itemObject = {
				itemType: item,
				itemData: {
					foundLocation: foundLocation,
					fullDetails: fullDetails
				}
			}
			
			if (colour) {
				itemObject.itemData.colour = colour;
			}
			if (storedLocation) {
				itemObject.itemData.storedLocation = storedLocation;
			}
			foundItemCache[id] = {time: Date.now(),
								  data: itemObject};
			break;

		case "add-found-item":
			responseText = "Success! Your item was added to the database.";
			await addFoundItem(id).catch(function(err) {
				responseText = "Error :( Please try again!";
				console.error(`Error awaitAndSendResponse: ${err}`);
			});
			break;

		case "incorrect-found-item":
			delete foundItemCache[id];
			break;

		case "search-for-item":
			const itemType = inputParams["search-items"].stringValue;
			const dbSearchResult = await searchForItem(itemType);
			console.log(dbSearchResult);
			if (!dbSearchResult) {
				responseText = "Sorry- nothing found!";
			} else {
				responseText = "The following lost items were reported:\n\n";
				let i = 1; // counter for indexing found items
				let keyboardButtons = []; // for user reply
				for (const key of Object.keys(dbSearchResult)) {
					responseText += `${i}: ${dbSearchResult[key].fullDetails}\n\n`;
					keyboardButtons.push(i.toString());
					i += 1;
				}
				responseText += "Select the item you wish to recover.";
				keyboardButtons.push("Cancel");
				const choiceKeyboard = {
						keyboard: [keyboardButtons],
						one_time_keyboard: true,
						resize_keyboard: true,
				};
				responseOptions.reply_markup = choiceKeyboard;
			}
			break;
		*/
		case "tell-joke":
			sendingStyle = "sendAsJoke";
			responseText = result.fulfillmentText.split('`');
			break;

		case "help-message":
			responseText = helpMessage;
			break;

		default:
			responseText = null;
			responseOptions = null;
	}

	return {message: responseText,
			options: responseOptions,
			sendingStyle: sendingStyle}
}

async function processCallbackQuery(query) {
	const chatId = query.from.id;
	const queryId = query.id;
	const messageId = query.message.message_id;
	let callbackData = query.data;
	
	let responseText = '';
	let sendingStyle = null;
	let responseOptions = {parse_mode: "Markdown"};
	
	switch (callbackData.split(' ')[0]) {
		
		case "menuComponent":
		
			callbackData = callbackData.split(' ');
			const menuItems = menuFeedbackCache[chatId][callbackData[1]];
            menuFeedbackCache[chatId] = menuFeedbackCache[chatId][callbackData[1]]
			bot.editMessageText(query.message.text, {
				chat_id: chatId,
				message_id: messageId,
				reply_markup: responseOptions
			});
			const itemsKeyboard = {
								       inline_keyboard: []
								   };
			let i = 0;
			menuItems.forEach(function(menuItem) {
				if (menuItem !== "_or_") {
					itemsKeyboard.inline_keyboard.push([{
						text: menuItem,
						callback_data: "menuItem " + i
					}]);
				}
				i++;
			});
			
			responseOptions.reply_markup = itemsKeyboard;
			responseText = "Please select the item you would like to give feedback on.";
			
			bot.sendMessage(chatId, responseText, responseOptions);
			break;
			
		case "menuItem":
			// send to Dialogflow in order to create and log context
			const msg = {chat: {id: chatId},
						  text: callbackData
			}
			awaitAndSendResponse(msg);
								
			bot.editMessageText(query.message.text, {
				chat_id: chatId,
				message_id: messageId,
				reply_markup: responseOptions
			});
			break;
			
		default:
		
	}
}
///* helper functions *///

async function sendMultipleMessages(chatId, msgs, delay) {
	bot.sendMessage(chatId, msgs.shift());
	for (const i in msgs) {
		bot.sendChatAction(chatId, "typing");
		await sleep(delay);
		bot.sendMessage(chatId, msgs[i]);
		await sleep(500);
	}
}

function formatDateMsg(dateString) {
	const days = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];
	const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	const dateObj = new Date(dateString);
	dateString = dateString.toString().split("T")[0].split("-");
	return {
		day: days[dateObj.getDay()],
		date: dateObj.getDate() + ' ' + months[dateObj.getMonth()],
	};
}

function formatDateRangeDb(startString, endString) {
	const dateList = [];
	let tempDate = new Date(startString);
	let endDate = new Date(endString);
	while (tempDate <= endDate) {
		const tempDateString = tempDate.getFullYear() + '-'
								+ (tempDate.getMonth() + 1) + '-' // JS months start from 0
								+ (tempDate.getDate());
		const tempDateDay = tempDate.getDay();
		dateList.push([tempDateString, tempDateDay]);
		tempDate.setDate(tempDate.getDate() + 1);
	}
	return dateList;
}

function getJSessionID() {
	const url = creditsURL + "login.do";
	
	// find and return "jsessionid=" value from HTML content
}

function checkBalance(id) {
	const url = creditsURL + "studstaffMealBalance.do;jsessionid=" + id;
	
	// find and extract data from HTML content
}
