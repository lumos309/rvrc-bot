// functions that are too long for the main program file

const constants = require('./constants.js');
const request = require('request-promise');
const parser = require('node-html-parser');
const aboutMessage = constants.aboutMessage;
const helpMessage = constants.helpMessage;
const startMessage = constants.startMessage;
const yesNoKeyboard = constants.yesNoKeyboard;
const creditsURL = constants.creditsURL;
const menuURL = constants.menuURL;

/* Takes in a week description string and optionally an array representing 
days of the week. Returns an array of arrays, each of which contains 
a week name string and the corresponding dates that match the input
days array. */
function formatAcadDate(weeks, days = [0, 1, 2, 3, 4, 5, 6], conjunction = null) {
	// AY18/19 Sem 2
	const weeksOfSem = [
		"Week 1",
		"Week 2",
		"Week 3",
		"Week 4",
		"Week 5",
		"Week 6",
		"Recess week",
		"Week 7",
		"Week 8",
		"Week 9",
		"Week 10",
		"Week 11",
		"Week 12",
		"Week 13",
		"Reading week",
		"Exam week 1",
		"Exam week 2"
	];
	
	const everyWeek = [
		"Week 1",
		"Week 2",
		"Week 3",
		"Week 4",
		"Week 5",
		"Week 6",
		"Week 7",
		"Week 8",
		"Week 9",
		"Week 10",
		"Week 11",
		"Week 12",
		"Week 13",
	];
	const everyEvenWeek = [
		"Week 2",
		"Week 4",
		"Week 6",
		"Week 8",
		"Week 10",
		"Week 12",
	];
	const everyOddWeek =[
		"Week 1",
		"Week 3",
		"Week 5",
		"Week 7",
		"Week 9",
		"Week 11",
		"Week 13",
	];
	switch (weeks) {
		case "every week":
			weeks = everyWeek;
			break;
		
		case "odd week":
			weeks = everyOddWeek;
			break;
		
		case "even week":
			weeks = everyEvenWeek;
			break;

		default: // specific weeks
			if (weeks.length === 1) {
				weeks = weeks[0].structValue.fields;
				weeks = weeks['number-integer']
							? [weeks['semester-week'].stringValue + ' ' + weeks['number-integer'].numberValue]
							: [weeks['semester-week'].stringValue];
			} else {
				const startWeek = weeks[0].structValue.fields;
				const endWeek = weeks[1].structValue.fields;
				const weekType = startWeek['semester-week'].stringValue;
				weeks = [];
				if (conjunction === "to") {
					for (let i = startWeek['number-integer'].numberValue;
						 i <= endWeek['number-integer'].numberValue;
						 i ++) {
							 weeks.push(weekType + ' ' + i);
						 }
				} else {
					weeks.push(weekType + ' ' + startWeek['number-integer'].numberValue);
					weeks.push(weekType + ' ' + endWeek['number-integer'].numberValue);
				}
			}
	}

	const startDate = "14 Jan 2019"; // week 1 Monday
	const result = [];

	for (const i in weeks) {
		const weekResult = [weeks[i]];
		const offset = weeksOfSem.indexOf(weeks[i]);
		const currWeek = new Date(startDate);
		currWeek.setDate(currWeek.getDate() + offset * 7);
		const selectedDate = currWeek.toDateString();
		for (const j in days) {
			const tempDay = days[j] === 0 ? 6 : days[j] - 1;
			const tempDate = new Date(selectedDate);
			tempDate.setDate(tempDate.getDate() + tempDay);
			tempDate.setHours(tempDate.getHours() + 8); // because GMT settings aren't working
			weekResult.push(tempDate);
		}
		result.push(weekResult);
	}
	
	return result;
}

/* Takes in an array of either days or a date period. Returns an array 
containing corresponding days of the week (0 = Sunday, 6 = Saturday). */
function datesToDays(inputDays) {
	const result = [];
	if (inputDays[0].structValue.fields.date) {
		for (const i in inputDays) {
			const tempDate = new Date(inputDays[i].structValue.fields.date.stringValue);
			result.push(tempDate.getDay());
		}
	} else {
		const datePeriod = inputDays[0].structValue.fields['date-period'].structValue.fields;
		const startDay = new Date(datePeriod.startDate.stringValue).getDay();
		const endDate = new Date(datePeriod.endDate.stringValue);
		const endDay = endDate.getDay() > startDay
							? endDate.getDay()
							: endDate.getDay() + 7;			
		for (let i = startDay; i <= endDay; i++) {
				result.push(i % 7);
		}
	}
	return result;
}

async function getAndParseMenu(inputDate, meal, format) {
	let body;
	await request(menuURL + inputDate)
		.then(function (htmlString) {
			body = htmlString;
		})
		.catch(function (err) {
			
		});
	if (((meal === "dinner") && (inputDate[1] === 6)) || (meal === "breakfast") && (inputDate[1] === 0)) {
		return '';
	} else if ((meal === "dinner") && (inputDate[1] !== 0)) {
		body = body.slice(body.indexOf('<tbody>') + 1);
		body = body.slice(body.indexOf('<tbody>'));
		body = body.slice(0, body.indexOf('</tbody>'));
	} else {
		body = body.slice(0, body.indexOf('</tbody>'));
	}
	const exp1 = /\r/g;
	const exp2 = /\n/g;
	const exp3 = /&nbsp;/g;
	const exp4 = /&amp;/g;
	body = body.replace(exp1, '');
	body = body.replace(exp2, '');
	body = body.replace(exp3, '');
	body = body.replace(exp4, '&');
	body = body.split('<tr><td class="imgtd" width="25%">').slice(1);
	body = meal === "breakfast"  // remove salad bar, grab and go, and toast stations
		       ? body.slice(1, -1)
			   : body.slice(1);
	const stations = [], stationMenus = [];
	body.forEach(function (row) {
		const items = row.split('</td><td>');

		// parse station name
		let station = items[0];
		station = station.split(".png")[0];
		station = station.split("/").pop();
		
		// parse food items
		let foods = items[1];
		foods = foods.slice(0, -10);
		
		// split by <p> tags
		foods = foods.split("</p><p>");
		
		// split by <br> tags
		let foodList = [];
		foods.forEach(function (item) {
			item = item.split("<br />");
			item.forEach(function (item1) {
				item1 = item1.replace("<p>", '');
				item1 = item1.replace("</p>", '');
				foodList.push(item1);
			})
		})
		
		stations.push(station);
		stationMenus.push(foodList);
	})
	
	let response;
	stationNames = {asian: "*ASIAN*",
					extra: "*EXTRA*",
					muslim: "*MUSLIM*",
					noodle: "*NOODLE*",
					indian: "*INDIAN*",
					specials: "*SPECIALS*",
					veg: "*VEGETARIAN*",
					western: "*WESTERN*"};
	if (format === "fullMenu") {
		response = '';
		for (const i in stations) {
			response += stationNames[stations[i]] + '\n';
			for (const j in stationMenus[i]) {
				response += formatMenuLanguage(stationMenus[i][j]) + '\n';
			}
			response += '\n';
		}
	} else if (format === "menuComponents") {
		const stationList = [], formattedStationMenus = [];
		stations.forEach(function(station) {
			stationList.push(stationNames[station]);
		});
		stationMenus.forEach(function(stationMenu) {
			const temp = [];
			stationMenu.forEach(function(menuItem) {
				temp.push(formatMenuLanguage(menuItem));
			});
			formattedStationMenus.push(temp);
		});
		response = [stationList, formattedStationMenus];
	}
	return response;
}

function formatMenuLanguage(string) {
	if (string == "OR") {
		return "_or_";
	} else {
		const replacements = {'Redish': "Radish",
							   'Hainasene': "Hainanese",
							   'W ': "with ",
							   'W/ ': "with ",
							   'With ': "with "};
		string = string.split(' ');
		let outputMenuString = '';
		string.forEach(function(word) {
			word = word.slice(0, 1) + word.slice(1).toLowerCase();
			outputMenuString += ' ' + word;
		})
		Object.entries(replacements).forEach(
			([key, value]) => outputMenuString = outputMenuString.replace(key, replacements[key])
		);
		return outputMenuString.slice(1);
	}
}

// returns a string in YYYY-MM-DD format
function formatDateMenu(inputDate) {
    var resultRegex = /\d+-\d+-\d+/; // YYYY-MM-DD
    var dateObjectRegex = /[A-Za-z]+ \d+ \d+/; // Date object
    if (inputDate.toString().match(resultRegex) !== null) return inputDate.match(resultRegex)[0];
    if (inputDate.toString().match(dateObjectRegex) !== null) {
        let dateStr = inputDate.getDate().toString();
        dateStr = dateStr.length === 2 ? dateStr : '0' + dateStr;
        let monthStr = (inputDate.getMonth() + 1).toString(); // JS months start from 0
        monthStr = monthStr.length === 2 ? monthStr : '0' + monthStr;
        return inputDate.getFullYear() + '-' + monthStr + '-' + dateStr;
    }
}

function checkTimeout(cachedData) {
	return (Date.now() - cachedData.time) / (1000 * 60 * 10) >= 20;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports.formatAcadDate = formatAcadDate;
module.exports.datesToDays = datesToDays;
module.exports.getAndParseMenu = getAndParseMenu;
module.exports.formatDateMenu = formatDateMenu;
module.exports.checkTimeout = checkTimeout;
module.exports.sleep = sleep;
