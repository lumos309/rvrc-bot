// constants

const constants = {
	aboutMessage: "About RVRC Bot:\n"
				   + "Version: Alpha v3.0\n"
				   + "Released: 22 Feb '19\n"
				   + "Status: Added menu feedback system\n"
				   + "Created by @lumos309",
	helpMessage: "Hello. I'm RVRC Bot! Here are some things I can help you with:\n\n"
				  + 'Menu check: "[Breakfast/dinner] [today/tomorrow/next Monday]".\n\n'
				  + 'Meal cancellation: "Cancel [breakfast/dinner/both meals]..."\n'
				  + '"... tomorrow."\n'
				  + '"... every odd Thursday."\n'
				  + '"... 1st to 3rd Feb."\n'
				  + '"... during reading week."\n\n'
				  + '"Display cancelled meals."\n\n'
				  + 'Feedback on meals: "Feedback dinner yesterday"\n\n'
				  + "... and more coming soon!\n\n"
				  + "Or for some fun, ask me about the weather or for a joke.\n\n"
				  + "If you run into trouble, just say 'restart' anytime to start over.",
    startMessage: "Welcome! I'm a chatbot designed for all RVRC residents.\n"
				  + "I'm still in development, but feel free to play around with "
				  + "my current features. Ask me who I am to find out more, or type"
				  + "/about or /help.",
	yesNoKeyboard: {
						keyboard: [["Yes", "No"]],
						one_time_keyboard: true,
						resize_keyboard: true,
				   },
	creditsURL: 'https://aces.nus.edu.sg/Prjhml',
	menuURL: 'http://hg.sg/nus_ohs_admin/adminOHS/backend/script/index.php?controller=pjFront&action=pjActionLoadEventDetail&index=4455&cate=0&dt=',
}

Object.entries(constants).forEach(
    ([key, value]) => module.exports[key] = value
	);
